public class SmartCallView{
    public SmartCall_View__c SmartCallViewIns{get;set;}
    public string SelectedFields {get;set;}
    public list<lead> LeadLst {get;set;}
    public list<lead> UpdateLeadLst {get;set;}
    public list<contact> ContactLst {get;set;}
    public list<contact> UpdateContactLst {get;set;}
    public string SelectedObject {get;set;}
    
    public boolean show_test{get; set;}
    public string testVar{get; set;}
    
    
    public list<selectoption> ViewLst{get;set;}
    public string ViewID {get;set;}
    public string whereClause {get;set;}
    public list<string> selectedfieldListname{get;set;}
    public integer count {get;set;}
    public list<WrapperForBinding> LeadWrapperLst {get;set;}
    public list<WrapperForBinding> ContactWrapperLst {get;set;}
    
    
    //Contructor
    public SmartCallView(){
    
        LeadWrapperLst = new list<WrapperForBinding>();
        ContactWrapperLst = new list<WrapperForBinding>();
        selectedfieldListname = new list<string>();
        ContactLst= new list<contact>();
        UpdateContactLst = new list<contact>();
        LeadLst = new list<Lead>();
        UpdateLeadLst = new list<lead>();
        ViewLst = new list<selectoption>();
        
        show_test = false;
        
        SmartCallViewIns = new SmartCall_View__c();
        
        //adding all Views in List for selecting View
        ViewLst.add(new SelectOption('', 'Select a View'));
        for(SmartCall_View__c svc : [Select Name, Id From SmartCall_View__c ])
        {
        
        

        ViewLst.add(new SelectOption(svc.id,svc.name));
    
        }
        
        
        BuildDynamicQuerry();
     }
     
    
    //Method which made the Dynamic query with respect to the view which has been selected 
    public void BuildDynamicQuerry(){
        
        boolean actionCheck=false;
        LeadLst = new list<lead>();
        ContactLst = new list<contact>();   
        selectedfieldListname = new list<string>();
        
        
        if(viewID<>null){
            //Getting  Fields, Conditions and Object whihc has selectedagainst particular View
            SmartCallViewIns=[Select (SELECT Field_Name__c,Field_Value__c,Operator__c,Field_Type__c FROM Setup_Smart_List__r),(Select Name,field_name__c, API__c, Object_Name__c, SmartCall_View__c From Fields_To_Show__r) 
                              From SmartCall_View__c s where s.id=:ViewID ];
        }
        system.debug('check!stQuerry'+SmartCallViewIns.Fields_To_Show__r);
        
        SelectedFields='';
        boolean checkFieldqoma=true;
        whereClause='';
        
        //Adding All fields into Single string for Dynamic Querry
        for(Fields_To_Show__c FieldIns : SmartCallViewIns.Fields_To_Show__r){
            if(checkfieldqoma==false)
            SelectedFields +=',';
            SelectedFields += FieldIns.API__c;
            SelectedObject = FieldIns.Object_Name__c;
             selectedfieldListname.add(FieldIns.API__c);
             checkfieldqoma=false;  
        }
        system.debug('fieldch'+selectedfieldListname);
        
        boolean checkAnd=true;
        
        //Adding All where conditions into single string for Dynamic querry
        for(Setup_Smart_List__c whereConditionIns :SmartCallViewIns.Setup_Smart_List__r){
            if(checkAnd==false)
                whereClause+=' and ';
                
            //All the filters with respect to operators
            if(whereConditionIns.Operator__c=='equals'){
          
                if(whereConditionIns.Field_Type__c=='Boolean' || whereConditionIns.Field_Type__c=='Currency' || whereConditionIns.Field_Type__c=='double')
                {
                     whereClause+=whereConditionIns.Field_Name__c+' = '+whereConditionIns.Field_Value__c;
                }
                else if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' = '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+' = '+'\''+String.escapeSingleQuotes(whereConditionIns.Field_Value__c)+'\'';
                }
            }
            if(whereConditionIns.Operator__c=='not equal to'){
             //By Zain Abbas Kazmi
                if(whereConditionIns.Field_Type__c=='Boolean' || whereConditionIns.Field_Type__c=='Currency' || whereConditionIns.Field_Type__c=='double')
                {
                     whereClause+=whereConditionIns.Field_Name__c+' != '+whereConditionIns.Field_Value__c;
                }
                else if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' != '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+ ' != '+'\''+String.escapeSingleQuotes(whereConditionIns.Field_Value__c)+'\'';
                }
            }
            if(whereConditionIns.Operator__c=='starts with')
            {
                whereClause+=whereConditionIns.Field_Name__c+' like '+'\''+String.escapeSingleQuotes(whereConditionIns.Field_Value__c)+'%\'';
            }
            if(whereConditionIns.Operator__c=='contains')
            {
                whereClause+=whereConditionIns.Field_Name__c+' like '+'\'%'+whereConditionIns.Field_Value__c+'%\'';
            }
            if(whereConditionIns.Operator__c=='does not contain')
            {
                whereClause+='not('+whereConditionIns.Field_Name__c+' like '+'\'%'+whereConditionIns.Field_Value__c+'%\''+')';
            }
            if(whereConditionIns.Operator__c=='less than')
            {
                if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' < '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+' < '+whereConditionIns.Field_Value__c;
                }
                
            }
            if(whereConditionIns.Operator__c=='greater than')
            {
                if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' > '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+' > '+whereConditionIns.Field_Value__c;
                }
            }
            if(whereConditionIns.Operator__c=='less or equal')
            {
                if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' <= '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+' <= '+whereConditionIns.Field_Value__c;
                }
                
            }
            if(whereConditionIns.Operator__c=='greater or equal')
            {
                if(whereConditionIns.Field_Type__c=='DateTime')
                {
                    whereClause+=whereConditionIns.Field_Name__c+' >= '+whereConditionIns.Field_Value__c+'T00:00:00.000Z';
                }
                else
                {
                    whereClause+=whereConditionIns.Field_Name__c+' >= '+whereConditionIns.Field_Value__c;
                }
            }
            
          checkAnd=false; 
          system.debug('checkWhere'+whereClause);
         // whereClause+= ' and ' + SelectedObject+'.Excluded_from_view__c=false';
         // whereClause+= ' and ' + SelectedObject+'.Excluded_from_view__c=false';
        } 
        system.debug('CheckFields'+SelectedFields);
        system.debug('checkwhere'+whereClause);
        
        // string of Dynamic querry
        string contactQuery = 'select '+ SelectedFields+ ' from '+ SelectedObject + ' '+'where' +' '+ whereClause ;
        system.debug('checkQuery'+ contactQuery);
        
        //Filling list with respect to querry
        if(ViewID !=null){
            if(SelectedObject=='lead'){
            
                LeadLst = Database.query(contactQuery );
            } 
            else{
           
                ContactLst = Database.query(contactQuery );   
            }
        }
        system.debug('checkContact'+ContactLst);
        system.debug('checkLead'+LeadLst);
        
        //checking the data with respect to query
        if(ContactLst.size()==0 && LeadLst.size()==0&ViewID!=null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Searching filter does not match with Record');
                ApexPages.addMessage(myMsg);
                system.debug('checking size'+ContactLst.size()+'checkingLead'+LeadLst.size());
        }
        
        //Binding lead list with checkbox into wrapper
        if(LeadLst.size()>0)
        {
            LeadWrapperLst = new list<WrapperForBinding>();
        
            for(lead leadIns: LeadLst){
                LeadWrapperLst.add(new WrapperForBinding( leadIns,actioncheck));
            }
            system.debug('testLeadwrap'+LeadWrapperLst );
        }
        
        //Binding Contact list with checkbox into wrapper
        if(ContactLst.size()>0)
        {
            ContactWrapperLst = new list<wrapperForBinding>();
            for(contact ContactIns: contactLst){
                ContactWrapperLst.add(new WrapperForBinding(ContactIns,actioncheck));
            }
        system.debug('testContactwrap'+ContactWrapperLst);
        }
    }
    
    
    //Method which block the checked records
    public void BlockMethod(){
    boolean checkbox = false;
        //Updating lead checked records
        if(LeadWrapperLst.size()>0){
            for(WrapperForBinding wrapBindLeadIns : LeadWrapperLst ){
                if(wrapBindLeadIns.actioncheck==true){
                    checkbox =true;
                    wrapBindLeadIns.LeadInsWrapper.Excluded_from_view__c=true;
                    system.debug('chekingleadIns'+wrapBindLeadIns.LeadInsWrapper);
                    UpdateLeadLst.add(wrapBindLeadIns.LeadInsWrapper);
                }
            }
            system.debug('checkUpdatedLeadList'+UpdateLeadLst);
            update UpdateLeadLst;
            if(UpdateLeadLst.size()>0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Records of '+SelectedObject+' has been blocked');
                ApexPages.addMessage(myMsg);
            }
        }
        
        //Updating Contct checked records
        if(ContactWrapperLst.size()>0){
            for(WrapperForBinding wrapBindContactIns : ContactWrapperLst ){
                if(wrapBindContactIns.actioncheck==true){
                    checkbox =true;
                    wrapBindContactIns.ContactInsWrapper.Excluded_from_view__c=true;
                    system.debug('chekingleadIns'+wrapBindContactIns.ContactInsWrapper);
                    UpdateContactLst.add(wrapBindContactIns.ContactInsWrapper);
                }
            }
            system.debug('checkUpdatedContactList'+UpdateContactLst);
            update UpdateContactLst;
            if(UpdateContactLst.size()>0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Records of '+SelectedObject+' has been blocked');
                ApexPages.addMessage(myMsg);
            }
        }
        
        // Clearing the instance for getting just new records
        LeadWrapperLst = new list<WrapperForBinding>();
        ContactWrapperLst = new list<wrapperForBinding>();
        
        //calling method for filling the list with new filtered record
        BuildDynamicQuerry();
        if(checkbox==false){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please check atleast one record for performing actions');
                ApexPages.addMessage(myMsg);
        }
    }
    
    
    //Navigate to the page of creating views
    public PageReference CreateViewPage(){
        String newPageUrl = 'https://c.ap1.visual.force.com/apex/SetupNewQuickList';
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage; 
    }
    
    
    // Wrapper class for binding Lead and Contact with checkbox of reminder and Remove form list
    public class WrapperForBinding{
        public lead LeadInsWrapper {get;set;}
        public contact ContactInsWrapper {get;set;}
        public boolean actioncheck {get;set;}
       
        //Nonparameterize Constructor
        public WrapperForBinding(){
        }
        
        //Constructor for binding Lead list with check box
        public WrapperForBinding(lead LeadInsWrapper , boolean actioncheck){
            this.LeadInsWrapper=LeadInsWrapper;
            this.ContactInsWrapper=ContactInsWrapper;
            this.actioncheck=actioncheck;
        } 
        
        //Constructor for binding Contact list with check box
        public WrapperForBinding(contact ContactInsWrapper ,  boolean actioncheck){
            this.LeadInsWrapper=LeadInsWrapper;
            this.ContactInsWrapper=ContactInsWrapper;
            this.actioncheck=actioncheck;
        }
    }
    public void showPopUp(){
        show_test = true;
        testVar= ApexPages.CurrentPage().getParameters().get('nickname');
        
        system.debug('ZAID MR. SLEEPY :)  --- '+testVar);
    }
    
    public void hidePopUp(){
        show_test = false;
        }
        
   
}