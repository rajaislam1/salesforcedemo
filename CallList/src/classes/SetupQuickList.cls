public class SetupQuickList
{
 
 
   public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    Integer count = 0;
 
 
 
    
public List<Setup_Smart_List__c> wrappers {get; set;}
public String conditionValue {get;set;}
public String operatorValue {get;set;}
public String fieldInputValues{get;set;}
public List<String>selectedFields {get;set;}
public List<SelectOption>fieldPicklist {get;set;}

public List <SelectOption> fieldNames{public get; private set;}
public List <SelectOption> setupConditions{public get; private set;}
public String selectedObject {get; set;}
public String defaultCondition {get; set;}
public String conditionId {get; set;}
public contact cont {get; set;}

//created by Zain Abbas Kazmi
    public List<SelectOption> fieldValue {get;set;}
    public List<SelectOption> fieldValue_items {get;set;}
    public string fieldType {get;set;}
    public string defaultValue {get;set;}
    public boolean isMethodCall {get; set;}
    
    
    public boolean lDate {get;set;} 
    public boolean lBoolean {get;set;} 
    public boolean lCurrency {get;set;} 
    public boolean lString {get;set;} 
    public boolean lEmail {get;set;} 
    public boolean lPhone {get;set;} 
    public boolean lPickList {get;set;}
    
    public string selectedValue {get;set;}
    public string selectedValue_items {get;set;}
    public string choosenObject {get; set;}
    Map<String, Schema.SObjectField> desResult;

//public string selectedObject {get; set;}

private Date conditionDate;

 public void saveFields() {
     
     //delete new List<Fields_To_Show__c>([select Id from Fields_To_Show__c]);
     
     List<String> selectedFieldsSave = fieldInputValues.split(';');
     List<Fields_To_Show__c> insertFields=new List<Fields_To_Show__c>();
     
         
      for(String Str : selectedFieldsSave)
      {
        Fields_To_Show__c fts=new Fields_To_Show__c();                 
        fts.API__c=str;  
        insertFields.add(fts);     
      }
        insert insertFields;    
    }
    
public SetupQuickList()
    { 
        wrappers = [select ID,Operator__c,Field_Name__c,Field_Value__c from Setup_Smart_List__c];   
        

        for(Setup_Smart_List__c setupList : wrappers)
        {
        if (setupList.Field_Name__c=='call_date__c')
                setupList.Field_Name__c='Did not call since '+setupList.Field_Value__c+' and more days' ;
        }

   
        //fieldNames = initObjNames();
        //getSelectedObj();
        //getFlds();
    }
    
public List<SelectOption> getSelectedObj()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('Lead','Lead'));
        options.add(new SelectOption('Contact','Contact'));
        return options;     
    }
private List<SelectOption> initObjNames() 
{
   
    //String type='Contact';
    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    Schema.SObjectType leadSchema = schemaMap.get(selectedObject );
    Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
    List<SelectOption> objNames = new List<SelectOption>();

    for (String fieldName: fieldMap.keySet()) {
    //if(! fieldName.contains('jigsaw'))    
        objNames.add(new SelectOption(fieldName,fieldMap.get(fieldName).getDescribe().getLabel()));
    }
   
    objNames.sort();
    return objNames;
}
    
public void removeCondition() 
    {
        List<Setup_Smart_List__c> removeCondition = [SELECT Id From Setup_Smart_List__c where ID=:conditionId];          
        delete removeCondition;     
    }
 
public void savededefaultCondition() 
    {
        Setup_Smart_List__c ssl=new Setup_Smart_List__c();
        ssl.Field_Name__c=defaultCondition;
        //ssl.Field_Value__c=String.valueOf((Date.today().addDays(Integer.ValueOf(condtionValue)*-1)));
        ssl.Field_Value__c=conditionValue;
        ssl.Operator__c=operatorValue;
        ssl.Records_to_show__c=10;
        insert ssl;
    }  
       
public void saveCondition() 
    {
        SmartCall_View__c view = new SmartCall_View__c();
        view.name = selectedObject;
        insert view;
        Setup_Smart_List__c ssl=new Setup_Smart_List__c();
        ssl.SmartCall_View__c = view.id;        
        ssl.Field_Name__c=selectedObject;
        ssl.Field_Value__c=conditionValue;
        ssl.Operator__c=operatorValue;
        ssl.Records_to_show__c=10;        
        insert ssl;
    }
    
    //created by Zain Abbas Kazmi
    
    public PageReference getFlds()
    {
        //System.debug('#################### ConVariable-Object: ' + ConVariable.Object__c);  
        
        List<SelectOption> options = new List<SelectOption>();
        if(selectedObject!= Null) 
        {
            System.debug('#################### Inside field retrieval method. ');
            desResult = Schema.getGlobalDescribe().get(choosenObject).getDescribe().Fields.getMap();
            List<String> fieldList = new List<String>();
            fieldList.addAll(desResult.keySet());
            options = returnPickListFields(fieldList);
        }
        
        //System.debug('#################### Method run successfully. ');
        fieldValue = options;
        isMethodCall = true;
        return null;
    }
    public PageReference getFldType()
    {
        if(selectedValue != '--None--') 
        {
            //Map<String, Schema.SObjectField> M = Schema.getGlobalDescribe().get(ConVariable.Object__c).getDescribe().Fields.getMap();
            Map<String, Schema.SObjectField> M = desResult;
            schema.DescribeFieldResult F = m.get(selectedValue).getDescribe();
            fieldtype = F.getType().name();
            
            // set field types to false initially      
            lBoolean = lDate = lCurrency = lString = lEmail = lPhone = lPickList = false;
            if (F.getType() == Schema.DisplayType.Boolean){ lBoolean = true; }
            else if (F.getType() == Schema.DisplayType.Date || F.getType() == Schema.DisplayType.Datetime) { lDate = true; }
            else if (F.getType() == Schema.DisplayType.Currency) { lCurrency = true; }
            else if (F.getType() == Schema.DisplayType.String) {lString = true; }
            else if (F.getType() == Schema.DisplayType.email) {lEmail = true; }
            else if (F.getType() == Schema.DisplayType.phone) {lPhone = true; }
            else if (F.getType() == Schema.DisplayType.pickList) {
                lPickList = true; 
               // fieldValue_items = getPicklistValues(selectedObject ,selectedValue);
        
                
                //List<String> fieldList = new List<String>();
                //fieldList.addAll(desResult.keySet());
                //fieldValue_items = ();
                }
            //fieldType = 'Selected field type is ... ' + selectedValue;
        }
        System.debug('##### Types:' + string.valueof(lBoolean) + '-' + string.valueof(lDate)+'-' + string.valueof(lCurrency) ); 
        //return fieldType;
        //getType();
        return null;
    }
  /*  public static list<SelectOption> getPicklistValues(String obj, String fld){
        list<SelectOption> options = new list<SelectOption>();
        // Get the object type of the SObject.
        Schema.getGlobalDescribe().get(selectedObject).getDescribe().fields(fld).getdescribe()
        Schema.sObjectType objType = obj.getSObjectType(); 
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a : values)
        { 
            options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return options;
    } */
    
    private List<SelectOption> returnPickListFields(List<String> fList)
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new selectOption('--None--', '--None--'));
        for(integer i =0;i<fList.size();i++)
        {
            options.add(new selectOption(fList[i], fList[i]));
        }
        return options;
    }


                        
    public PageReference incrementCounter() {
            count++;
            return null;
    }
                    
public Integer getCount() {
        return count;
    }

}