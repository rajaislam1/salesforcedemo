public class SetupNewQuickListController 
{
    //public list<viewWrapperClass> lstOfMainWrapper {get; set;}
    public viewWrapperClass viewWrapperItem {get; set;}
    
    public List<SelectOption> fieldValue {get;set;}
    public List<SelectOption> fieldValue_items {get;set;}
    public List<SelectOption> fieldSelected {get; set;}
    public Boolean isDisplay_Section2 {get;set;}
    
    public List<String> leftselected {get; set;}
    public List<String> rightselected {get; set;}
    
    public string DeaultFields {get;set;}
    
    public string viewName {get; set;}
    public string selectedValue {get;set;}
    public string selectedValue_items {get;set;}
    public string fieldType {get;set;}
    public String operatorValue {get;set;}
    public String conditionValue {get;set;}
    public string choosenObject {get; set;}
    public String conditionId {get; set;}
    public string fieldID {get; set;}
    public string ViewId {get; set;}
    public string emailField {get; set;}
    public string phoneField {get; set;}
    
    
    public boolean displayPopup {get; set;}
    
    
    public string rowindexnumber {get; set;}
    public integer rowIndex {get; set;}
    
    public list<smartcall_view__c> lstOfViews {get; set;}
    
    public boolean addMore {get; set;}
    public boolean showHeader {get; set;}
    
    
    Set<string> leftvalues = new Set<string>();
    Set<string> rightvalues = new Set<string>();
    
    
    public boolean isMethodCall {get; set;}
    public boolean hideViews {get; set;}
    
    
    
    
    
    public List<Setup_Smart_List__c> wrappers {get; set;}
    public list<Fields_To_Show__c> lstOfFieldstoShow {get; set;}
    public List<Fields_To_Show__c> insertFields {get;set;}
    
    public map<string,string> leftSelectedMap {get; set;}
    public map<string,string> rightSelectedMap {get; set;}  
  
    public list<SmartCall_View__c> lstOfView {get; set;}
    
    Map<String, Schema.SObjectField> desResult;
    
    
    Integer count = 0;
    
    public SetupNewQuickListController()
    {
        hideViews = false;
        leftselected= new List<String>();
        rightselected = new List<String>();
        leftSelectedMap = new map<string,string>();
        rightSelectedMap = new map<string,string>();
        
        
        DeaultFields = 'First Name,Last Name,Account,Phone';
        insertFields=new List<Fields_To_Show__c>();
        
        viewWrapperItem = new viewWrapperClass();
        //lstOfMainWrapper = new list<viewWrapperClass>();
        lstOfViews = new list<smartcall_view__c>();
        
        reRenderOnSave();
    }
    
    public void reRenderOnSave()
    {
         wrappers = [select ID,Operator__c,Field_Name__c,Field_Value__c,Field_Type__c,Object__c,SmartCall_View__r.name from Setup_Smart_List__c];   
        

        for(Setup_Smart_List__c setupList : wrappers)
        {
        if (setupList.Field_Name__c=='call_date__c')
                setupList.Field_Name__c='Did not call since '+setupList.Field_Value__c+' and more days' ;
        }
        lstOfFieldstoShow = [select SmartCall_View__r.name,API__c,Object_Name__c,Field_Name__c from Fields_To_Show__c];
        
    }
    public pageReference getViewOnClick()
    {
        lstOfViews = [select name,(select Operator__c,Field_Name__c,Field_Value__c,Field_Type__c,Object__c,SmartCall_View__r.name from Setup_Smart_List__r),
                             (select SmartCall_View__r.name,API__c,Object_Name__c,Field_Name__c from Fields_To_Show__r) from smartcall_view__c where id=:ViewId];
        system.debug('#####dddddddd'+lstOfViews);
        showHeader = true;
        return null;
    }
    
    public void hideViewPb()
    {
        hideViews = true;
    }
    
    public void showAllViews()
    {
        lstOfView = [select name,id from SmartCall_View__c];
        hideViews = false;
    }
    
    public void removeCondition() 
    {
        List<Setup_Smart_List__c> removeCondition = [SELECT Id From Setup_Smart_List__c where ID=:conditionId];          
        delete removeCondition;
        reRenderOnSave();     
    }
    
    public void removeFieldsToShow()
    {
        list<Fields_To_Show__c> removeFields = [SELECT Id From Fields_To_Show__c where ID=:fieldID];
        delete removeFields;
        reRenderOnSave();
    }
    
    public void removeView()
    {
        SmartCall_View__c viewIns = [select id from SmartCall_View__c where id=:ViewId];
        delete viewIns;
        showAllViews();
        reRenderOnSave(); 
    }
    
    public  PageReference ViewList(){
        String newPageUrl = 'https://c.ap1.visual.force.com/apex/SmartCallViewVF';
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage; 
    
    
    }
    
    
    public list<SelectOption> getOperatorValues()
    {
        list<selectOption> options = new list<selectOption>();
        
        options.add(new SelectOption('equals ','equals'));
        options.add(new SelectOption('not equal to','not equal to'));
        options.add(new SelectOption('starts with','starts with'));
        options.add(new SelectOption('contains','contains'));
        options.add(new SelectOption('does not contain','does not contain'));
        options.add(new SelectOption('less than','less than'));
        options.add(new SelectOption('greater than','greater than'));
        options.add(new SelectOption('less or equal','less or equal'));
        options.add(new SelectOption('greater or equal','greater or equal'));        
        options.add(new SelectOption('within','within'));
        return options;
    }
    
    public list<SelectOption> getRadioValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('true','true'));
        options.add(new SelectOption('false','false'));
        return options;
        
    }
    
    public List<SelectOption> getSelectedObj()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('--None--','--None--'));
        options.add(new SelectOption('Lead','Lead'));
        options.add(new SelectOption('Contact','Contact'));
        return options;     
    }
    
   public void addMoreCondition()
   {
    addMore = true;
   } 
    
    
     public PageReference getFlds()
    {
        System.debug('#################### ConVariable-Object: ' + choosenObject);  
        
        //viewWrapperItem = null;
        conditionWrapperClass conditionWrapperItem = null;
        //lstOfMainWrapper = new list<viewWrapperClass>();
        SmartCall_View__c SmartView = null;
        
        
        
        List<SelectOption> options = new List<SelectOption>();
        if(choosenObject!= Null) 
        {
            if(choosenObject != '--None--')
            {
                isDisplay_Section2 = true;
                System.debug('#################### Inside field retrieval method.');
                desResult = Schema.getGlobalDescribe().get(choosenObject).getDescribe().Fields.getMap();
                System.debug('#################### desResult'+desResult);
                List<String> fieldList = new List<String>();
                List<String> fieldAPIList = new List<String>();
                leftvalues.clear();
                rightvalues.clear();
                
                leftSelectedMap = new map<string,string>();
                rightSelectedMap = new map<string,string>();
                
                for (String fieldName: desResult.keySet()) {
                    System.debug('##Field API Name='+fieldName);// list of all field API name
 
                    fieldList.add(desResult.get(fieldName).getDescribe().getLabel());//It provides to get the object fields label.
                    
                    if(choosenObject=='Lead')
                    {
                         if(desResult.get(fieldName).getDescribe().getLabel()!='First Name' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Last Name' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Account ID' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Rating' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Phone' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Status'&&
                            desResult.get(fieldName).getDescribe().getLabel()!='--None--')
                        {
                            leftvalues.add(desResult.get(fieldName).getDescribe().getLabel());
                            system.debug('$$$$$$$$$$$leftvalues'+leftvalues);
                            system.debug('????????????api'+ desResult.get(fieldName).getDescribe().getName());
                            leftSelectedMap.put(desResult.get(fieldName).getDescribe().getLabel(),desResult.get(fieldName).getDescribe().getName());
                            system.debug('$$$$$$$$$$$leftSelectedMap'+leftSelectedMap);
                        }
                        else
                        {
                            rightvalues.add(desResult.get(fieldName).getDescribe().getLabel());
                            system.debug('$$$$$$$$$$$rightvalues'+rightvalues);
                            rightSelectedMap.put(desResult.get(fieldName).getDescribe().getLabel(),desResult.get(fieldName).getDescribe().getName());
                        }
                        
                    }
                    else
                    {
                         if(desResult.get(fieldName).getDescribe().getLabel()!='First Name' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Last Name' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Account ID' &&
                            desResult.get(fieldName).getDescribe().getLabel()!='Phone'&&
                            desResult.get(fieldName).getDescribe().getLabel()!='--None--')
                        {
                            leftvalues.add(desResult.get(fieldName).getDescribe().getLabel());
                            leftSelectedMap.put(desResult.get(fieldName).getDescribe().getLabel(),desResult.get(fieldName).getDescribe().getName());
                            system.debug('$$$$$$$$$$$leftSelectedMap'+leftSelectedMap);
                        }
                        else
                        {
                            rightvalues.add(desResult.get(fieldName).getDescribe().getLabel());
                            rightSelectedMap.put(desResult.get(fieldName).getDescribe().getLabel(),desResult.get(fieldName).getDescribe().getName());
                        }
                    }
                    
                   
                    //leftvalues.add(desResult.get(fieldName).getDescribe().getLabel());
                    
                } 
                System.debug('#################### fieldList'+fieldList);
                fieldAPIList .addAll(desResult.keySet());
                //leftvalues.addAll(desResult.keySet());
                System.debug('#################### fieldList'+fieldList);
                options = returnPickListFields(fieldList,fieldAPIList);
                isMethodCall = true;
                
                
                viewWrapperItem = new viewWrapperClass();
                SmartView = new SmartCall_View__c(name='');
                viewWrapperItem.ViewIns = SmartView;
                fieldValue = options;
                fieldValue.sort();
                
                for(integer wrapperlistcounter = 0; wrapperlistcounter<5;wrapperlistcounter++)
                {
                    
                    conditionWrapperItem = new conditionWrapperClass();
                    conditionWrapperItem.selectedFieldName = '';
                    conditionWrapperItem.operatorfieldValue = '';
                    conditionWrapperItem.selectedFieldValue = '';
                    conditionWrapperItem.selectedWrapperRowIndex = wrapperlistcounter;
                    conditionWrapperItem.selectedFieldList = null;
                    viewWrapperItem.lstOfConditions.add(conditionWrapperItem);
                }
                //selectedValue = 'Industry';
                //lstOfMainWrapper.add(viewWrapperItem);
                System.debug('#################### fieldValue'+fieldValue);
                
                
            }
            else
            {
                isMethodCall = false;
            }
        }
        
        //System.debug('#################### Method run successfully. ');
        
       
        
        
       
       
        return null;
    }
    
    public PageReference getFldType()
    {
        //system.debug('lstOfMainWrapper:'+lstOfMainWrapper);
       // if(selectedValue != '--None--') 
        //{
            
            rowindexnumber = ApexPages.CurrentPage().getParameters().get('selectedrowindex');
            rowIndex = integer.valueof(rowindexnumber);
            selectedValue = ApexPages.CurrentPage().getParameters().get('selectedwrappervalue');
            if(selectedValue!='--None--')
            {
                Map<String, Schema.SObjectField> M = desResult;
                system.debug('<<<<<<<<<<<<<selectedValue:'+selectedValue);
                schema.DescribeFieldResult F = M.get(selectedValue).getDescribe();
                fieldtype = F.getType().name();
                system.debug('<<<<<<<<<<<<<fieldtype '+fieldtype );
                
                //for(viewWrapperClass viewWrapperItem :viewWrapperItem.lstOfConditions)
                //{
                    for(conditionWrapperClass conditionWrapperItem :viewWrapperItem.lstOfConditions)
                    {
                        if(conditionWrapperItem.selectedWrapperRowIndex == rowIndex)
                        {
                            conditionWrapperItem.fieldTypeName = fieldtype;
                            system.debug('@@@@@@@@@FT:'+conditionWrapperItem.fieldTypeName);
                             conditionWrapperItem.lBoolean = conditionWrapperItem.lDate = conditionWrapperItem.lCurrency = conditionWrapperItem.lString = conditionWrapperItem.lEmail = conditionWrapperItem.lPhone = conditionWrapperItem.lRichText = conditionWrapperItem.lID = conditionWrapperItem.lPickList = conditionWrapperItem.lDouble = conditionWrapperItem.lURL = false;
                            if (F.getType() == Schema.DisplayType.Boolean){conditionWrapperItem.lBoolean = true; }
                            else if (F.getType() == Schema.DisplayType.Date || F.getType() == Schema.DisplayType.Datetime) { conditionWrapperItem.lDate = true; }
                            else if (F.getType() == Schema.DisplayType.Currency) { conditionWrapperItem.lCurrency = true; }
                            else if (F.getType() == Schema.DisplayType.String) {conditionWrapperItem.lString = true; }
                            else if (F.getType() == Schema.DisplayType.email) {conditionWrapperItem.lEmail = true; }
                            else if (F.getType() == Schema.DisplayType.phone) {conditionWrapperItem.lPhone = true; }
                            else if (F.getType() == Schema.DisplayType.TextArea){conditionWrapperItem.lRichText = true;}
                            else if (F.getType() == Schema.DisplayType.Reference) {conditionWrapperItem.lID = true;}
                            else if (F.getType() == Schema.DisplayType.URL) {conditionWrapperItem.lURL = true;}
                            else if (F.getType() == Schema.DisplayType.Double) {conditionWrapperItem.lDouble = true;}
                            else if (F.getType() == Schema.DisplayType.PICKLIST) {
                            conditionWrapperItem.lPickList = true;
                            fieldValue_items = getDefaultPicklistValues();
                            system.debug('@@@@@@@@@@@@fieldValue_items'+fieldValue_items);
                    
                    
                            }
                
            System.debug('##### Types:' + string.valueof(conditionWrapperItem.lBoolean) + '-' + string.valueof(conditionWrapperItem.lDate)+'-' + string.valueof(conditionWrapperItem.lCurrency) );
                        }
                     }
                  //}
                
                }
           
            
        return null;
    }
    
  
    public List<selectOption> getDefaultPicklistValues()
    {
        List<selectOption> PicklistValues = new List<selectOption>();
        Map<String, Schema.SObjectField> M = desResult;
        Schema.DescribeFieldResult MyPicklistFieldDescribeRslt;
        MyPicklistFieldDescribeRslt = M.get(selectedValue).getDescribe();
        List<Schema.PicklistEntry> MyFieldPicklistValues = MyPicklistFieldDescribeRslt.getPicklistValues();
        
        for(Schema.PicklistEntry pfield: MyFieldPicklistValues)
        {
            
            PicklistValues.add(new SelectOption (pfield.getValue(), pfield.getLabel()) );
            // If the user had not selected any picklist value for this field, then set its value hardcoded.
            //if(NewPickListVal)
              //  cv.Default_Value__c = pfield.getValue();
        }
        //NewPickListVal = true;
        return PicklistValues;
    }
    
    
    private List<SelectOption> returnPickListFields(List<String> fList,List<String> fAPIList)
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new selectOption('--None--', '--None--'));
        for(integer i =0;i<fList.size();i++)
        {
            if(fList[i]!='Address' && fList[i]!='Jigsaw Contact ID' && fList[i]!='Description')
            options.add(new selectOption(fAPIList[i], fList[i]));
        }
        return options;
    }
    
    public PageReference selectclick(){

        rightselected.clear();

        for(String s : leftselected)
        {

            leftvalues.remove(s);
           

            rightvalues.add(s);
            rightSelectedMap.put(s,leftSelectedMap.get(s));
            leftSelectedMap.remove(s);

        }

        return null;

    }

     

    public PageReference unselectclick(){

        leftselected.clear();

        for(String s : rightselected){

            rightvalues.remove(s);
            

            leftvalues.add(s);
            leftSelectedMap.put(s,rightSelectedMap.get(s));
            rightSelectedMap.remove(s);

        }

        return null;

    }


    public List<SelectOption> getunSelectedValues(){

        List<SelectOption> options = new List<SelectOption>();

        List<string> tempList = new List<String>();

        tempList.addAll(leftvalues);

        tempList.sort();

        for(string s : tempList)

            options.add(new SelectOption(s,s));

        return options;

    }
        
    
    

    public List<SelectOption> getSelectedValues()
    {

        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
        {
            options1.add(new SelectOption(s,s));
        }
        return options1;

    }
    
    public void closePopup() 
    {  
        boolean addComma=false;
        if(viewWrapperItem.lstOfConditions!=null)
        {
            for(conditionWrapperClass cwc :viewWrapperItem.lstOfConditions)
            {
                system.debug('@@@@@selectedFieldList:'+cwc.selectedFieldList);
                for(string s:cwc.selectedFieldList)
                {
                    if(addComma==false)
                    {
                        cwc.Picklistfield  = s;
                        addComma = true;
                    }
                    else
                    {
                        cwc.Picklistfield += ','+s;
                    }
                
                }
            }
        }      
        
        displayPopup = false;    
    }     
    
    
    public void showPopup() 
    {        
        displayPopup = true;    
    }
    
    public void cancelPopup()
    {
        displayPopup = false;
    }
   /* public PageReference saveCondition() 
    {
        try {
            SmartCall_View__c view = new SmartCall_View__c();
            view.name = viewName + '(' + choosenObject + ')';
            insert view;
            Setup_Smart_List__c ssl=new Setup_Smart_List__c();
            ssl.SmartCall_View__c = view.id;        
            ssl.Field_Name__c=selectedValue;
            if (lBoolean == true)
            {
                ssl.Field_Value__c = string.valueof(booleanField);
            }
            else if (lString == true)
             {
                 ssl.Field_Value__c = conditionValue;
             }
             else if (lDate == true)
             {
                 ssl.Field_Value__c = string.valueof(dateField);
             }
             else if (lEmail == true)
             {
                 ssl.Field_Value__c = string.valueof(emailField); 
             }
             else if (lPhone == true)
             {
                 ssl.Field_Value__c = string.valueof(phoneField); 
             }
             else if (lPickList == true)
             {
                 ssl.Field_Value__c = conditionValue;
             }
             else
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Condition Value');
                 ApexPages.addMessage(myMsg);
             }
             //ssl.Field_Value__c = conditionValue;
             ssl.Operator__c=operatorValue;
             ssl.Records_to_show__c=10; 
                
             system.debug('ssl.Field_Value__c'+ssl.Field_Value__c+'conditionValue'+conditionValue);   
             upsert ssl;
             List<Fields_To_Show__c> insertFields=new List<Fields_To_Show__c>();
             system.debug('%%%%%%%%%%%%rightselected'+rightvalues);
             for(string selectedfield :rightvalues)
             {
                 Fields_To_Show__c fts=new Fields_To_Show__c();                 
                 fts.API__c=selectedfield;
                 fts.object_Name__c = choosenObject;
                 fts.SmartCall_View__c = view.id;
                 insertFields.add(fts);     
             }
             upsert insertFields;
             reRenderOnSave();
               
        }
        catch (exception ex){
            // display error msg
             //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
             //ApexPages.addMessage(myMsg);
        } 
      return null;
      }*/
       
      public void SaveWrapper()
      {
        
        boolean isRollBack = false;
        
        SmartCall_View__c viewInsToSave;
        Setup_Smart_List__c CoditionsToSave;
        list<Setup_Smart_List__c> lLstOfCondtions;
        List<Fields_To_Show__c> insertFieldstoShow=new List<Fields_To_Show__c>();
        
        //system.debug('^^^^lstOfMainWrapper::'+lstOfMainWrapper);
        
        
        // Create a savepoint
        Savepoint sp = Database.setSavepoint();
        if(choosenObject != '--None--' || (viewName!=null && viewName<> '' && !string.isblank(viewName)))
        {
            try
            {
                
                //for (viewWrapperClass vwc:lstOfMainWrapper)
                //{
                    viewInsToSave = new SmartCall_View__c();
                    viewInsToSave.name = viewName;
                    upsert viewInsToSave;
                    
                    system.debug('viewWrapperItem:'+viewWrapperItem);
                    
                    for(conditionWrapperClass cwc :viewWrapperItem.lstOfConditions)
                    {
                            system.debug('viewWrapperItem.lstOfConditions:'+viewWrapperItem.lstOfConditions);
                            if((cwc.selectedFieldValue!=null && cwc.selectedFieldValue<> '' && !string.isblank(cwc.selectedFieldValue)) ||
                            (cwc.dateField!=null)||(cwc.selectedFieldList!=null) && cwc.selectedFieldName!='--None--' )
                            {
                                // transaction flag set to TRUE
                                isRollBack = true;
                                CoditionsToSave = new Setup_Smart_List__c();
                                CoditionsToSave.SmartCall_View__c = viewInsToSave.id;
                                CoditionsToSave.Field_Name__c = cwc.selectedFieldName;
                                if(cwc.lDate)
                                {
                                    CoditionsToSave.Field_Value__c =string.valueOf(Date.valueOf(cwc.dateField));
                                }
                                else if(cwc.lPickList)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.Picklistfield; 
                                            
                                }
                                else if(cwc.lString)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.selectedFieldValue;
                                }
                                
                                else if(cwc.lCurrency)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.currencyField;
                                }
                                else if(cwc.lDouble)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.doubleField;
                                }
                                else if(cwc.lEmail)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.emailField;
                                }
                                else if(cwc.lID)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.IdField;
                                }
                                else if(cwc.lURL)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.URLField;
                                }
                                else if(cwc.lBoolean)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.booleanField;
                                }
                                else if(cwc.lPhone)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.phoneField;
                                }
                                else if(cwc.lRichText)
                                {
                                    CoditionsToSave.Field_Value__c = cwc.TextAreafield;
                                }
                                else
                                {
                                    system.debug('zzz');
                                }
                                CoditionsToSave.Operator__c =cwc.operatorfieldValue;
                               // CoditionsToSave.Object__c = choosenObject;
                                CoditionsToSave.Object__c = choosenObject;
                                CoditionsToSave.Field_Type__c = cwc.fieldTypeName; 
                                CoditionsToSave.Records_to_show__c = 10;
                                system.debug ('### CoditionsToSave:' +CoditionsToSave);
                                //lLstOfCondtions.add(CoditionsToSave);
                                upsert CoditionsToSave;
                                
                            }
                            //upsert lLstOfCondtions;
                            
                    }
                    
                    // check for Transaction flag
                                if(isRollBack)
                                {
                                    for(string selectedfieldtoShow :rightSelectedMap.keyset())
                                    {
                                         Fields_To_Show__c fts=new Fields_To_Show__c();                 
                                         fts.API__c=rightSelectedMap.get(selectedfieldtoShow);
                                         fts.Field_Name__c = selectedfieldtoShow;
                                         fts.object_Name__c = choosenObject;
                                         fts.SmartCall_View__c = viewInsToSave.id;
                                         
                                         insertFieldstoShow.add(fts);     
                                    }
                                    system.debug ('### insertFieldstoShow:' +insertFieldstoShow);
                                    upsert insertFieldstoShow;
                                    apexpages.addmessage(new apexpages.message(apexpages.severity.CONFIRM,'View saved successfully'));
                                    }
                                else
                                {       
                                    
                                    apexpages.addmessage(new apexpages.message(apexpages.severity.Error,'Enter Field Value'));
                                    Database.rollback(sp);
                            //addErrorMessage = true;
                                }
                          //}
                    system.debug ('### viewInsToSave:' +viewInsToSave);
                    
                    
                     reRenderOnSave();
                }
            
                catch(exception ex)
                {
                    Database.rollback(sp);
                    system.debug('Zain');
                    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'View already exists, enter a different name and try again');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage());
                    ApexPages.addMessage(myMsg);
                }
        }
        else
        {
            apexpages.addmessage(new apexpages.message(apexpages.severity.Error,'Choose Object and Select field'));
        }
        
        system.debug('viewWrapperItem1:'+viewWrapperItem);
                    
        
      }
      
      public class viewWrapperClass
      {
      
          public SmartCall_View__c ViewIns {get; set;}
          public list<selectoption> fieldlist {get; set;}
          public list<selectoption> fieldTypelist {get; set;}
          
          public list<conditionWrapperClass> lstOfConditions {get; set;}
          //public list<fieldsToShowWrapperClass> lstOfFieldsToShow {get; set;}
          
          
          public viewWrapperClass()
          {
              this.lstOfConditions = new list<conditionWrapperClass>();
              //this.lstOfFieldsToShow = new list<fieldsToShowWrapperClass>();
              
          }
      
      }
      
      
      
      
      public class conditionWrapperClass
      {
       
           public Setup_Smart_List__c ConditionIns {get; set;}
           public string selectedFieldValue {get; set;}
           public string selectedFieldName {get; set;}
           public integer selectedWrapperRowIndex {get; set;}
           public list<string> selectedFieldList {get; set;}
           public string operatorfieldValue {get; set;}
           public list<selectoption> fieldlist {get; set;}
           public boolean lDate {get;set;} 
           public boolean lBoolean {get;set;} 
           public boolean lCurrency {get;set;} 
           public boolean lString {get;set;} 
           public boolean lEmail {get;set;} 
           public boolean lPhone {get;set;} 
           public boolean lPickList {get;set;}
           public boolean lRichText {get;set;}
           public boolean lID {get;set;}
           public boolean lDouble {get;set;}
           public boolean lURL {get;set;}
           
           public string Picklistfield {get; set;}
           
           public string fieldTypeName {get; set;}
           
           public string booleanField {get; set;}
           public string emailField {get; set;}
           public string IdField {get; set;}
           public string phoneField {get; set;}
           public string currencyField {get; set;}
           public string doubleField {get; set;}
           public string URLField {get; set;}
           
           
           public string TextAreafield {get; set;}
           //public boolean booleanFieldFalse {get; set;}
           
           public date dateField {get; set;}
           
            
           
           
           public conditionWrapperClass() {
            selectedFieldList = new list<string>();
            
           }
           
           public conditionWrapperClass(string selectedFieldName,string operatorfieldValue,string selectedFieldValue,list<string> selectedFieldList)
           {
            this.selectedFieldName=selectedFieldName;
            this.operatorfieldValue=operatorfieldValue;
            this.selectedFieldValue=selectedFieldValue;
            this.selectedFieldList=selectedFieldList;
           }
       
       
       }
       
       
       /*public class fieldsToShowWrapperClass
       {
           public Fields_To_Show__c fieldToShowIns {get; set;}
       
       
       }*/
       
       
       
       
       
       
       
       
       
       
                        
 
}