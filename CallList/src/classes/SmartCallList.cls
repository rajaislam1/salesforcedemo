public class SmartCallList{   
       
    public Contact[] contacts {
        get {
            string str='SELECT '+FieldsToShow()+' FROM Contact where IsDeleted=false '+GetCondition();   
            System.Debug('*****'+str);        
            contactsInformation =database.query(str); 
            return Database.query('select ' + FieldsToShow() +' FROM Contact where IsDeleted=false '+GetCondition());
        }
        
    }
     
    public String[] fields {
        get {
            return parseCsv(FieldsToShow());
        }
    }
    
    private String[] parseCsv(String s) {
        List<String> l = new List<String>();
        for (String f : s.split(',')) {
            l.add(f.trim());
        }
        return l;
    }
  
       
  public List<contact> contactsInformation { get; set; }
  public SmartCallList(){
        
    //getCallingList();
    }

   public list<Account> lstAccount {get;set;} 
   
  public void contactAction()
  {
    
  }  
  
  private string GetCondition()
  {
    string whereClause='';
    string operator='';
    
    List<Setup_Smart_List__c> setupValues = [select Field_Value__c,Field_Name__c,Operator__c from Setup_Smart_List__c];  
    
    for(Setup_Smart_List__c whereConditions : setupValues)
    {     
         if (whereConditions.Operator__c=='equal') operator='=';
         if (whereConditions.Operator__c=='greater') operator='>';     
                
             if (whereConditions.Field_Name__c=='call_date__c')
             {
                //DateTime d = datetime.now();
                //d = d.format('MMMM dd, yyyy hh:mm:ss a');
                operator='<';
                DateTime dtValue=(Date.today().addDays(Integer.ValueOf(whereConditions.Field_Value__c)*-1));
                whereClause=whereClause+ ' and ('+ whereConditions.Field_Name__c+operator+dtValue.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                whereClause=whereClause+ ' or '+ whereConditions.Field_Name__c+' = NULL)';
             }
             else
                whereClause=whereClause+ ' and '+ whereConditions.Field_Name__c+operator+'\''+whereConditions.Field_Value__c+'\'';
    }
    return whereClause;
  }
  private string FieldsToShow()
  {
        
        List<Fields_To_Show__c> fetchFields = [select API__c from Fields_To_Show__c];  
        string fieldsToShow='';
        integer fieldCount=0;
        for(Fields_To_Show__c fieldsShow : fetchFields)
        {
            if (fieldCount==0)
                fieldsToShow=fieldsShow.API__c;
            else
                fieldsToShow=fieldsToShow+','+fieldsShow.API__c;
                            
            ++fieldCount;   
        }   
    return fieldsToShow;
  }   
  
   public void getCallingList() 
   {
             
        }    
}