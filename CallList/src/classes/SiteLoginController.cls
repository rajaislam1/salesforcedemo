/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}

   global pageReference login() {
          String startUrl = 'http://www.yahoo.com'; // Prod
          PageReference portalPage = new PageReference(startUrl);
          portalPage.setRedirect(true);
          PageReference p = Site.login(username, password, startUrl);
          if (p == null) {
                return Site.login(username, password, null);
          } else {
                return portalPage;
          }
 }

   
   
}